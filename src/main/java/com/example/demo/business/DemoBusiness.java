package com.example.demo.business;

import org.springframework.stereotype.Service;

@Service
public class DemoBusiness {
    
    public String hello (String value) {
        return "Bonjour " + value + " Bienvenue sur le projet gitpod";
    }
}
