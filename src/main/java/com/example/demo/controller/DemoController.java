
package com.example.demo.controller;

import com.example.demo.business.DemoBusiness;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {

    @Autowired
    DemoBusiness demoBusiness;

    @GetMapping("/hello/{value}")
    public ResponseEntity<String> helloWord(@PathVariable String value) {

        String returnValue = demoBusiness.hello(value);
    return new ResponseEntity<String>(returnValue,HttpStatus.OK);
}

}

